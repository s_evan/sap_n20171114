sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/UIComponent",
	"sap/ui/core/routing/History"
], function(Controller,JSONModel,UIComponent,History) {
	"use strict";

	return Controller.extend("n20171115.controller.processDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf n20171115.view.processDetail
		 */
		onInit: function(){
            var oRouter = UIComponent.getRouterFor(this);
            oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);    
        },

        onNavPress: function() {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash != undefined){
                window.history.go(-1);
            }else{
                var oRouter = UIComponent.getRouterFor(this);
                oRouter.navTo("list",{}, true);
            }
        },

        _onObjectMatched: function (oEvent) {            
            var RequisitionId = decodeURIComponent(oEvent.getParameter("arguments").RequisitionId);
            this._onGetDetail(RequisitionId);
        },
        _onGetDetail:function(sId){
            var ApiUrl = "/Flow7/api/fdp/m/"+sId;
        	var oProcessDetail = new JSONModel(ApiUrl);
			//oProcessList.setData("/Flow7/dashboard/processing","ProcessList");	
			this.getView().setModel(oProcessDetail);
            console.log(oProcessDetail);
        }

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf n20171115.view.processDetail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf n20171115.view.processDetail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf n20171115.view.processDetail
		 */
		//	onExit: function() {
		//
		//	}

	});

});