sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
    "sap/ui/core/UIComponent",
    'sap/m/MessageToast'
], function(Controller,JSONModel,UIComponent,MessageToast) {
	"use strict";

	return Controller.extend("n20171115.controller.processList", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf n20171115.view.processList
		 */
			onInit: function() {
				var oProcessList = new JSONModel("/Flow7/api/dashboard/processing");
				//oProcessList.setData("/Flow7/dashboard/processing","ProcessList");	
				this.getView().setModel(oProcessList);
				console.log(oProcessList);
			},
			onListItemPressed: function(oEvent){
                 var oItem, oCtx,oDiagramId;

				oItem = oEvent.getSource();
				oCtx = oItem.getBindingContext();
				oDiagramId = oCtx.getProperty("DiagramId");
				// this.getRouter().navTo("detail",{
				// 	RequisitionId : oCtx.getProperty("RequisitionId")
				// });
				if(oDiagramId==="FDP_P0"){
					var oRouter = UIComponent.getRouterFor(this);
	                 oRouter.navTo("detail", {
	                     RequisitionId: oCtx.getProperty("RequisitionId")
	                });
				}else{
					MessageToast.show("无法查看详情信息！");	
				}
			}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf n20171115.view.processList
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf n20171115.view.processList
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf n20171115.view.processList
		 */
		//	onExit: function() {
		//
		//	}

	});

});