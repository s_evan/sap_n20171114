sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("n20171115.controller.App", {
		onInit:function(){
			var oProcessList = new JSONModel("/Flow7/api/dashboard/processing");
			//oProcessList.setData("/Flow7/dashboard/processing","ProcessList");	
			this.getView().setModel(oProcessList,"Flow7");
			console.log(oProcessList);
		}
	});
});